import 'jquery-validation';
import store from 'shop-packages/redux/store';
import {
  setPaymentOption,
  setPayOnDeliveryMethod,
  completePayOnDeliveryMethod
} from 'shop-packages/redux/checkout/actions';
import {
  setAgreement,
  clearErrors
} from 'shop-packages/redux/checkout/reducer';
import observe, {
  getValue
} from 'shop-packages/redux/connectSelector';
import {
  selectPayOnDeliveryMethods,
  selectCurrentPayOnDeliveryMethod,
  selectAgreement,
  selectPending,
  selectErrors,
} from 'shop-packages/redux/checkout/selectors';
import {
  templateRenderer
} from './utils';
import {
  formatPrice
} from 'pz-core/utils/formatter/price';

class PzPayOnDelivery {
  constructor({
    pk,
    formSelector = '#PayOnDeliveryForm',
    methodTemplateSelector = '#PayOnDeliveryMethodTemplate',
    methodItemInputSelector = '.js-pay-on-delivery-method',
    methodListSelector = '#PayOnDeliveryMethodList',
    agreementInputSelector = '.js-checkout-agreement-input',
    completeButtonSelector = '#PayOnDeliveryCompleteButton'
  }) {
    this.observers = [];
    this.selectedMethod = null;
    store.dispatch(setPaymentOption(pk));

    this.extractor = {
      methodItemInputSelector: methodItemInputSelector,
    };

    this.$form = $(formSelector);
    this.$methodTemplate = $(methodTemplateSelector).html();
    this.$methodList = $(methodListSelector);
    this.$agreementInput = $(agreementInputSelector);
    this.$completeButton = $(completeButtonSelector);

    this.$agreementInput.on('input.agreement', this.onAgreementChange.bind(this));

    this.initObservers();
    this.initPayOnDeliveryForm();
  }

  onSelectedMethodUpdate(selectedMethod) {
    if (!selectedMethod) {
      return;
    }

    this.selectedMethod = selectedMethod;
    this.$methodList
      .find(`${this.extractor.methodItemInputSelector}[value="${this.selectedMethod}"]`)
      .prop('checked', true);
  }

  onMethodsUpdate(methods) {
    this.methods = methods;
    const content = this.methods.map(({ value, label, price }) => {
      const checked = this.selectedMethod === value ? 'checked' : '';
      return templateRenderer(this.$methodTemplate, {
        value,
        label,
        price: formatPrice(price),
        checked,
      });
    });

    this.$methodList.html(content.join(''));
    this.$methodList
      .find(this.extractor.methodItemInputSelector)
      .on('input', (e) => {
        const selected = $(e.currentTarget).val();
        store.dispatch(setPayOnDeliveryMethod(selected));
      });
  }

  onAgreementChange(e) {
    store.dispatch(
      setAgreement($(e.currentTarget).is(':checked'))
    );
  }

  onAgreementUpdate(agreement) {
    this.$agreementInput.prop('checked', agreement);
  }

  onPendingUpdate(isPending) {
    isPending ?
      this.$completeButton
        .attr('disabled', 'disabled')
        .addClass('loading') :
      this.$completeButton
        .removeAttr('disabled')
        .removeClass('loading');
  }

  onErrorsUpdate(errors) {
    $('.js-error-*').text('').hide();
    for (const key in errors) {
      $(`.js-error-${key}`).text(errors[key]).show();
    }
  }

  initPayOnDeliveryForm() {
    this.validator = this.$form.validate({
      submitHandler: (_, event) => {
        event.preventDefault;

        store.dispatch(clearErrors());
        store.dispatch(completePayOnDeliveryMethod());

        return false;
      },
      highlight: (element) => {
        const $element = $(element);

        if ($element.attr('type') === 'checkbox') {
          $element.parent().addClass('has-errors');
        }

        $element.addClass('has-errors');
      },
      unhighlight: (element) => {
        const $element = $(element);

        if ($element.attr('type') === 'checkbox') {
          $element.parent().removeClass('has-errors');
        }

        $element.removeClass('has-errors');
      },
      errorPlacement: (error, element) => {
        const $element = $(element)[0];
        if ($element.name === 'agreement') {
          error.insertAfter(element);
        }

        return false;
      },
      rules: {
        agreement: {
          required: true,
          normalizer: () => getValue(selectAgreement),
        }
      }
    });
  }

  initObservers() {
    this.observers = [
      observe(selectCurrentPayOnDeliveryMethod).subscribe(this.onSelectedMethodUpdate.bind(this)),
      observe(selectPayOnDeliveryMethods).subscribe(this.onMethodsUpdate.bind(this)),
      observe(selectAgreement).subscribe(this.onAgreementUpdate.bind(this)),
      observe(selectPending).subscribe(this.onPendingUpdate.bind(this)),
      observe(selectErrors).subscribe(this.onErrorsUpdate.bind(this)),
    ];
  }

  unmount() {
    this.$agreementInput.off('input.agreement');
    store.dispatch(setAgreement(false));
    this.validator.destroy();
    for (const observer of this.observers) {
      observer.unsubscribe();
    }
  }
}

export default PzPayOnDelivery;
