# pz_pay_on_delivery
A library module for enabling "Pay On Delivery" payment module at order/payment step.  

It **doesn't work** in projects where the "Pay On Delivery" feature is not active.  

## Usage
### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;  

```bash
-e git+https://git@bitbucket.org/akinonteam/pz_pay_on_delivery.git@000000#egg=pz_pay_on_delivery
# "000000" is the latest commit id or hash
```

Note: always make sure to use the latest commit id in the above example.  
For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).  

Next, run the following command to install the package.  

```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package

```bash
# in /templates
yarn add git+ssh://git@bitbucket.org:akinonteam/pz_pay_on_delivery.git#000000
# or
npm i git+ssh://git@bitbucket.org:akinonteam/pz_pay_on_delivery.git#000000
# "000000" is the latest commit id or hash
```

Make sure to use the same git commit id as in `requirements.txt`.  

### 3. Add to the project

```python
# omnife_base/settings.py:
INSTALLED_APPS.append('pz_pay_on_delivery')
```

### 4. Import template:

```jinja
{# in templates/orders/payment-tab.html #}
{% from 'pz_pay_on_delivery/index.html' import PayOnDeliveryForm %}

{# example using scenario at tabs structure #}

{# should use where there are tab buttons #}
<button class="js-payment-tab" data-type="pay_on_delivery">Kapıda Ödeme</button>

{# should use where there are tab contents #}
<div class="js-payment-tab-content" data-type="pay_on_delivery">
  {{ PayOnDeliveryForm() }}
</div>
```

### 5. Import and initialize JS

```js
// in templates/orders/checkout/paymentTab.js
import PayOnDeliveryOption from 'pz_pay_on_delivery';

class PaymentTab {
  paymentOptions = new Map([
    // other payment options ...
    [ 'pay_on_delivery', PayOnDeliveryOption ],
  ]);
  // ...
}
```

### 6. Import styles:

```scss
@import "~pz_pay_on_delivery/";
```

## Customizing the Html (Jinja Macro)
```jinja
{# Simple Using #}
{{ PayOnDeliveryForm() }}

{# Using w/ Custom Template #}
{% call PayOnDeliveryForm() %}
  {# ...custom html elements... #}
{% endcall %}

{# Simple w/ Passing Parameters (Default Parameters) #}
{{ PayOnDeliveryForm(
  formId='PayOnDeliveryForm',
  formClass=false,
  description=false,
  aggrementModal='#salesModal') }}
```

All of the following are optional parameters for the Jinja2 macro.  

- **formId**: (String)  
- **formClass**: (String)  
- **description**: (String)  
- **aggrementModal**: (String)  

## JS settings
Pass an object, which will be taken as destructured parameters, to customize things on the JS class.  

```js
// w/ Defaults
new PzPayOnDelivery({
  pk,
  formSelector = '#PayOnDeliveryForm',
  methodTemplateSelector = '#PayOnDeliveryMethodTemplate',
  methodItemInputSelector = '.js-pay-on-delivery-method',
  methodListSelector = '#PayOnDeliveryMethodList',
  agreementInputSelector = '.js-checkout-agreement-input',
  completeButtonSelector = '#PayOnDeliveryCompleteButton'
});
```

- **pk**: (String, *Required*) Payment Option Primary Key  
- **formSelector**: (String)  
- **methodTemplateSelector**: (String)  
- **methodItemInputSelector**: (String)  
- **methodListSelector**: (String)  
- **agreementInputSelector**: (String)  
- **completeButtonSelector**: (String)  
